﻿namespace PortableDeviceMarketplace
{
    public class Product
    {
       public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int ImagePath
        {
            get;
            set;
        }

        public string ID
        {
            get;
            set;
        }
    }
}
