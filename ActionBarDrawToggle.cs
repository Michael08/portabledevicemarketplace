﻿using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using SupportActionBarDrawToggle = Android.Support.V7.App.ActionBarDrawerToggle;

namespace PortableDeviceMarketplace
{
    public class ActionBarDrawToggle : SupportActionBarDrawToggle
    {
        protected AppCompatActivity mHostActivity;
        protected int mOpenedResource;
        protected int mClosedResource;

        public ActionBarDrawToggle(AppCompatActivity host, DrawerLayout drawerLayout, int openedResource, int closedResource) 
            : base(host, drawerLayout, openedResource, closedResource)
        {
            mHostActivity = host;
            mOpenedResource = openedResource;
            mClosedResource = closedResource;
        }

        public override void OnDrawerOpened(View drawerView)
        {
            base.OnDrawerOpened(drawerView);
        }

        public override void OnDrawerClosed(View drawerView)
        {
            base.OnDrawerClosed(drawerView);
        }

        public override void OnDrawerSlide(View drawerView, float slideOffset)
        {
            base.OnDrawerSlide(drawerView, slideOffset);
        }
    }
}
