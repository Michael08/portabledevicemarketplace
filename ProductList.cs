﻿using System.Collections.Generic;
using System.Linq;

namespace PortableDeviceMarketplace
{
    public class ProductList : IProductList
    {
        static Product ClickedItem;

        public List<Product> Products => new List<Product>
                {
                    new Product
                    {
                        Name = "Samsung S8",
                        Description = "Samsung Galaxy S8 smartphone was launched in March 2017. The phone comes with a 5.80-inch touchscreen display with a resolution of 1440 pixels by 2960 pixels at a PPI of 570 pixels per inch. Samsung Galaxy S8 price in India starts from Rs. 45,990. The Samsung Galaxy S8 is powered by 1.9GHz octa-core processor and it comes with 4GB of RAM. The phone packs 64GB of internal storage that can be expanded up to 256GB via a microSD card. As far as the cameras are concerned, the Samsung Galaxy S8 packs a 12-megapixel primary camera on the rear and a 8-megapixel front shooter for selfies.",
                        ImagePath = @Resource.Drawable.SamsungS8,
                        ID = "1"
                    },

                    new Product
                    {
                        Name = "IPhone 8",
                        Description = "The iPhone 8 and iPhone 8 Plus are smartphones designed, developed, and marketed by Apple Inc. They were announced on September 12, 2017, alongside the higher-end iPhone X, at the Steve Jobs Theater in the Apple Park campus, and were released on September 22, 2017, succeeding iPhone 7 and iPhone 7 Plus. Besides the addition of a glass back, the designs of iPhone 8 and 8 Plus are largely similar to that of their predecessors. Notable changes include the addition of wireless charging, a faster processor, and improved cameras and displays. The iPhone 8 and 8 Plus share most of their internal hardware with the iPhone X.",
                        ImagePath  = @Resource.Drawable.IPhone8,
                        ID = "2"
            },

                    new Product 
                    {
                        Name = "IPad 5nd gen",
                        Description = "The iPad received mixed reviews. It was significantly praised for performance, with reviewers asserting that the model is noticeably faster than older iPad models, and it also received positive reviews for its price and battery life. It was criticized, however, for lacking a laminated and anti-reflective screen, and lack of support for Apple Pencil and attachable keyboards through the Smart Connector. Its introductory price in the United States was the lowest ever for an iPad, with the media noting that the lower price might be an effort to encourage wider adoption of the tablet in the education sector, as well as for businesses needing inexpensive tablets for undemanding uses.",
                        ImagePath  = @Resource.Drawable.IPad5thGen,
                        ID = "3"
            },
                    new Product
                    {
                        Name = "Getac Android Tablet",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque accumsan, purus viverra tincidunt molestie, magna magna tincidunt leo, a vestibulum tortor mi ut arcu. Donec ac mi id sapien tristique iaculis eget vel ante. In nec tellus ut ligula auctor pellentesque. Duis sollicitudin nisi sit amet ultrices consectetur. Donec pulvinar tortor eu ex ultrices, sit amet imperdiet lorem finibus. Nunc sodales augue sed lorem euismod, eu blandit metus suscipit. Cras eu neque luctus, malesuada enim et, ullamcorper massa. Vestibulum in quam sed nunc pellentesque commodo id rutrum lacus. Nunc in aliquam orci. Sed eget tempor massa, eget pretium felis. Vestibulum lacinia est quis tempus tincidunt.",
                        ImagePath  = @Resource.Drawable.getac,
                        ID = "4"
            },
                    new Product
                    {
                        Name = "IPhone 6",
                        Description = "In hac habitasse platea dictumst. Quisque a dui commodo, ultrices quam a, venenatis nisi. Aliquam imperdiet ante eu risus ultrices, sed aliquet dolor sagittis. Vivamus semper dapibus aliquet. Suspendisse sit amet lorem sapien. Donec mauris urna, tempus id odio sed, imperdiet vulputate ex. In aliquet dignissim odio, sed suscipit turpis posuere in. Nullam id mi eget enim ultricies ultricies at sed enim. Vestibulum a lacus posuere, finibus elit sed, malesuada nisi. ",
                        ImagePath  = @Resource.Drawable.getac,
                        ID = "5"
            },
                    new Product
                    {
                        Name = "IPhone X",
                        Description = "Nulla at lorem vitae metus lobortis facilisis. Quisque nulla orci, pellentesque id nibh a, consectetur vulputate neque. Quisque et gravida eros, id egestas nisi. Etiam placerat nunc vestibulum elit tempus, aliquam vulputate massa euismod. Nulla sollicitudin ipsum a mollis gravida. Fusce in lectus orci. Pellentesque nec nibh quis nulla varius scelerisque.",
                        ImagePath  = @Resource.Drawable.iPhoneX,
                        ID = "6"
            },
                    new Product
                    {
                        Name = "Pixel 2",
                        Description = "Nulla lacus ipsum, luctus vitae euismod sit amet, tincidunt et augue. Integer eros arcu, cursus vel ipsum laoreet, sodales auctor diam. Suspendisse lobortis faucibus libero at ornare. Nunc augue dui, efficitur ac egestas quis, lacinia vel dolor. Maecenas odio nulla, vulputate ut lectus a, pulvinar hendrerit eros. Aliquam erat volutpat. ",
                        ImagePath  = @Resource.Drawable.pixel2,
                        ID = "7"
            },
                    new Product
                    {
                        Name = "Samsung J5",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.samsungJ5,
                        ID = "8"
            },
                    new Product
                    {
                        Name = "Samsung Tab A7",
                        Description = "Quisque placerat libero ac fermentum tempor. Vivamus eros lorem, ornare et magna vitae, porta tempor sem. Donec sed odio malesuada, venenatis enim non, consectetur diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut non ipsum nibh. Nam eu fermentum nisi. Aenean lacinia ut mauris eu accumsan.",
                        ImagePath  = @Resource.Drawable.samsungTabA7,
                        ID = "9"
            },
                    new Product
                    {
                        Name = "Dummy Product 10",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "10"
            },

                    new Product
                    {
                        Name = "Dummy Product 11",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "11"
            },

                    new Product
                    {
                        Name = "Dummy Product 12",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "12"
            },
                new Product
                    {
                        Name = "Dummy Product 13",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "13"
            },
                new Product
                    {
                        Name = "Dummy Product 14",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "14"
            },
                new Product
                    {
                        Name = "Dummy Product 15",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "15"
            },
                new Product
                    {
                        Name = "Dummy Product 16",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "16"
            },
                new Product
                    {
                        Name = "Dummy Product 17",
                        Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur euismod a orci vitae laoreet. Ut volutpat ultrices nisi, sit amet faucibus ipsum congue non. Vivamus condimentum consectetur imperdiet. Praesent commodo a nisl sit amet dictum. Vestibulum lacus ipsum, sodales sed velit id, dictum tincidunt ligula. Donec maximus luctus felis, id varius tortor fermentum in. Morbi nec porttitor mauris, ut iaculis lectus. Duis ac sodales felis. /n Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse ac neque egestas, tincidunt velit in, rhoncus ex. Etiam lacinia vestibulum libero, id consequat metus. Vivamus feugiat lacus sed leo suscipit molestie. Vivamus quis interdum ligula. Cras nulla tellus, varius nec ligula sed, sagittis luctus eros. Curabitur eu massa odio. Cras ornare diam nec dolor blandit tempor.",
                        ImagePath  = @Resource.Drawable.placeholder,
                        ID = "17"
            }
       };

        public Product ReturnsClickedItem()
        {
            return ClickedItem;
        }

        public string ReturnsClickedItemId()
        {
            if (ClickedItem != null)
            {
                var Id = ClickedItem.ID;
                return Id;
            }
            else
            {
                return 0.ToString();
            }
        }

        public string ReturnsClickedItemName()
        {
            var Name = ClickedItem.Name;
            return Name;
        }

        public string ReturnsProductDetailDescription()
        {
            var Description = ClickedItem.Description;
            return Description;
        }

        public int ReturnsProductImagePath()
        {
            var ImagePath = ClickedItem.ImagePath;
            return ImagePath;
        }

        public void UpdateClickedItem(string Id)
        {
            var product = Products.FirstOrDefault(p => p.ID == Id);
            ClickedItem = product;
        }
    }
}