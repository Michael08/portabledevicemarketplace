﻿using System;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Autofac;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

namespace PortableDeviceMarketplace.Activities
{
    [Activity(Label = "ProductDetailActivity", Theme = "@style/MyTheme")]
    public class ProductDetailActivity : AppCompatActivity
    {
        readonly IProductList _products = App.Container.Resolve<IProductList>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ProductDetailScreen);

            var mToolBar = FindViewById<SupportToolbar>(Resource.Id.toolbarProductDetail);

            SetSupportActionBar(mToolBar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.Title = (_products.ReturnsClickedItemName());

            var itemDescription = FindViewById<TextView>(Resource.Id.productDetailDescription);
            itemDescription.Text = _products.ReturnsProductDetailDescription();

            var itemImage = FindViewById<ImageView>(Resource.Id.productDetailImage);
            itemImage.SetImageResource(_products.ReturnsProductImagePath());

            FindViewById<Button>(Resource.Id.addToCartBTN).Click += AddToCartnBtn_Click;
        }

        public override bool OnSupportNavigateUp()
        {
            OnBackPressed();
            return true;
        }

        protected override void OnStop()
        {
            FindViewById<Button>(Resource.Id.addToCartBTN).Click -= AddToCartnBtn_Click;
            base.OnStop();
        }

        public void AddToCartnBtn_Click(object sender, EventArgs e)
        {
            var products = App.Container.Resolve<IProductList>();
            var basket = App.Container.Resolve<IBasket>();

            basket.AddToBasket();

            var numOfItems = basket.GetQuantity(products.ReturnsClickedItem());
            Toast.MakeText(BaseContext, string.Format($"Item added to cart! ({numOfItems})"), ToastLength.Short).Show();
        }
    }
}
