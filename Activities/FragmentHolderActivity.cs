﻿using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using PortableDeviceMarketplace.Fragments;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using SupportFragment = Android.Support.V4.App.Fragment;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

namespace PortableDeviceMarketplace.Activities
{
    [Activity(Label = "FragmentHolderActivity", MainLauncher = false, Theme = "@style/MyTheme")]
    public sealed class FragmentHolderActivity : AppCompatActivity
    {
        SupportToolbar _mToolBar;

        ActionBarDrawerToggle _mDrawerToggle;
        DrawerLayout _mDrawerLayout;
        ListView _mLeftDrawer;
        ArrayAdapter _mNavigationPageAdapter;
        List<string> _navigationPages;

        SupportFragment _mDashboardFragment;
        SupportFragment _mBasketPageFragment;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.FragmentHolderLayout);

            SetupNavBar();

            CheckFirstTimeBeingLaunched();

            _mDashboardFragment = new DashboardFragment();
            _mBasketPageFragment = new BasketPageFragment();
            
            ShowFragment(_mDashboardFragment);
        }

        void ShowFragment(SupportFragment fragment)
        {
            var trans = SupportFragmentManager.BeginTransaction();
            trans.Replace(Resource.Id.fragmentContainer, fragment);
            trans.AddToBackStack(null);
            trans.Commit();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            _mDrawerToggle.OnOptionsItemSelected(item);
            return base.OnOptionsItemSelected(item);
        }

        void MLeftDrawer_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var positionOfItemClicked = e.Id;

            switch (positionOfItemClicked)
            {
                case 0:
                    ShowFragment(_mDashboardFragment);
                    break;
                case 1:
                    //Start MobilePhones activity
                    ShowAlert("Start Mobile Phones Activity, not done this yet");
                    break;
                case 2:
                    //Start Tablets activity
                    ShowAlert("Start Tablets Activity, not done this yet");
                    break;
                case 3:
                    ShowFragment(_mBasketPageFragment);
                    break;
            }

            _mDrawerLayout.CloseDrawers();
        }

        void ShowAlert(string alertMessage)
        {
            var alertDialog = new AlertDialog.Builder(this);
            alertDialog.SetTitle("Item Clicked");
            alertDialog.SetMessage(alertMessage);
            alertDialog.SetNeutralButton("Continue", delegate {
                alertDialog.Dispose();
            });
            alertDialog.Show();
        }

        void CheckFirstTimeBeingLaunched()
        {
            if (App.IsFirstTime == "yes")
            {
                App.IsFirstTime = "no";
                _mDrawerLayout.OpenDrawer(_mLeftDrawer);

                Snackbar.Make(_mLeftDrawer, "Welcome To Michael's Portable Device MarketPlace", Snackbar.LengthLong)
                    .Show();
            }
        }

        public void SetActionBarTitle(int title)
        {
            SupportActionBar.SetTitle(title);
        }

        public void SetupNavBar()
        {
            _navigationPages = new List<string>
            {
                "Home",
                "Mobile Phones",
                "Tablets",
                "Basket"
            };

            _mLeftDrawer = FindViewById<ListView>(Resource.Id.Left_Drawer);
            _mToolBar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
            _mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.Draw_Layout);

            SetSupportActionBar(_mToolBar);

            _mNavigationPageAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, _navigationPages);
            _mLeftDrawer.Adapter = _mNavigationPageAdapter;

            _mDrawerToggle = new ActionBarDrawerToggle(
                this,                       //Host Activity
                _mDrawerLayout,              //Drawer Layout    
                0,                          //No open drawer content
                0                           //No closed drawer content
            );

            _mDrawerLayout.AddDrawerListener(_mDrawerToggle);
            _mDrawerToggle.SyncState();

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SetActionBarTitle(Resource.String.dashboardTitle);

            _mLeftDrawer.ItemClick += MLeftDrawer_ItemClick;
        }
    }
}
