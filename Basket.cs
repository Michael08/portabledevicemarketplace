﻿using System.Collections.Generic;

namespace PortableDeviceMarketplace
{
    public class Basket : IBasket
    {
        readonly Dictionary<Product, int> _basket = new Dictionary<Product, int>();

        ProductList productList = new ProductList();

        public void AddToBasket()
        {
            var clickedItem = productList.ReturnsClickedItem();

            if (!_basket.ContainsKey(clickedItem) && clickedItem != null)
            {
                _basket.Add(clickedItem, 1);
            }
            else
            {
                var value = _basket[clickedItem];
                _basket[clickedItem] = value += 1;
            }
        }

        public bool BasketHasProducts()
        {
            bool result;

            if (_basket.Keys.Count >= 1)
            {
                result = true;
            }
            else
            {
                result = false;
            }

            return result;
        }

        public List<string> GetProductIds()
        {
            List<string> IdList = new List<string>();

            foreach (var item in GetProducts())
            {
                IdList.Add(item.ID);
            }

            return IdList;
        }

        public List<Product> GetProducts()
        {
            List<Product> ProductList = new List<Product>(_basket.Keys);

            return ProductList;
        }

        public int GetQuantity(Product product)
        {
            var value = _basket[product];
            return value;
        }

        public void RemoveFromBasket(Product product, int quantity)
        {
            _basket[product] =- quantity;
        }
    }
}
