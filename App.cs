﻿using Android.App;
using Autofac;

namespace PortableDeviceMarketplace
{
    public static class App
    {
        public static IContainer Container { get; set; }

        static App()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ProductList>().As<IProductList>();
            builder.RegisterInstance(new Basket()).As<IBasket>();
            Container = builder.Build();
        }

        public static string IsFirstTime
        {
            get { return Helpers.Settings.GeneralSettings; }
            set
            {
                if (Helpers.Settings.GeneralSettings == value)
                    return;
                Helpers.Settings.GeneralSettings = value;
            }
        }
    }
}
