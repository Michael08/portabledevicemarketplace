﻿namespace PortableDeviceMarketplace
{
    public interface IProductClickListener
    {
        void OnClick(int position);
    }
}