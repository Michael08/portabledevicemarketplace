﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using PortableDeviceMarketplace;

public class DashboardProductHolder : RecyclerView.ViewHolder
{
    TextView _labelItemName;
    public static string ItemName;

    protected IProductClickListener _clickListener;
    protected int _position;

    public DashboardProductHolder(View itemView, IProductClickListener listener)
        : base(itemView)
    {
        _clickListener = listener;

        _labelItemName = itemView.FindViewById<TextView>(Resource.Id.LabelItemName);
        _labelItemName.Click += ItemView_Click;
    }

    public void Setup(string text, int position)
    {
        _position = position;
        _labelItemName.Text = text ;
    }

    void ItemView_Click(object sender, EventArgs e)
    {
        _clickListener.OnClick(_position);
        ItemName += _labelItemName.Text;
    }
}
