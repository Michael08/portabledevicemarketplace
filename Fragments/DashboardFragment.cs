﻿using System.Linq;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Autofac;
using Java.Interop;
using PortableDeviceMarketplace.Activities;

namespace PortableDeviceMarketplace.Fragments
{
    public class DashboardFragment : Fragment, IProductClickListener
    {
        LinearLayout _linearLayout;
        ProductList productList = new ProductList();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            ((FragmentHolderActivity)Activity).SetActionBarTitle(Resource.String.dashboardTitle);
        }

        public override void OnResume()
        {
            base.OnResume();
            ((FragmentHolderActivity)Activity).SetActionBarTitle(Resource.String.dashboardTitle);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.DashboardScreen, container, false);
            var mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.ProductListRecyclerView);
            var mLayoutManager = new LinearLayoutManager(Context);
            mRecyclerView.SetLayoutManager(mLayoutManager);

            var mAdapter = new DashboardProductsAdapter(productList.Products.Select(p => p.Name).ToList(), this);
            mRecyclerView.SetAdapter(mAdapter);

            _linearLayout = (LinearLayout)view.FindViewById(Resource.Id.linearLayout1);

            return view;
        }

        [Export("ChangeBackgroundColor")]
        public void ChangeBackgroundColor()
        {
            _linearLayout.SetBackgroundColor(Android.Graphics.Color.Aqua);
        }

        public void OnClick(int position)
        {
            var selectedId = productList.Products.Select(p => p.ID).ToList()[position];
            var products = App.Container.Resolve<IProductList>();

            if (products.ReturnsClickedItemId() != selectedId)
            {
                products.UpdateClickedItem(selectedId);
            }

            StartActivity(new Intent(Activity, typeof(ProductDetailActivity)));
        }
    }
}
